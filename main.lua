--[[
Batto! A clicking arcade game
Copyright 2018 Pajo <xpio at tut dot by>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]

-- love 11.1

function love.load()
	love.window.setMode(1080/2.5, 1920/2.5)
	screen = {
		w = love.graphics.getWidth(),
		h = love.graphics.getHeight()
	}

	player = {
		x = screen.w / 2,
		y = screen.h * 3 / 4,
		size = screen.w / 6,
		draw = function(self)
			love.graphics.setColor(.5, .5, .8)
			love.graphics.circle("fill", self.x, self.y, self.size / 2)
		end,
		move = function(self, x, y)
			if trails[1] then return end -- no movement until all trails expire
			trails:make(x, y, self.x, self.y)
			player.x = x
			player.y = y
		end,
		destroy = function(self)
			game.score = game.score - 10
			explosions:make(self.x, self.y, self.size)
		end,
	}

	trails = {
		min = 1,
		max = 40,
		duration = 1 / 20,
		timer = 0,
		make = function(self, x, y, sx, sy)
			local expiration = 10 
			local d = ((x - sx) ^ 2 + (y - sy) ^ 2) ^ 0.5
			local howmany = math.max(math.min(d / expiration, self.max), self.min)
			local step
			for a = self.min, howmany do
				step = 1 - a / howmany
				trails[a] = {
					size = player.size,
					x = sx - (sx - x) * step,
					y = sy - (sy - y) * step
				}
			end
		end,
		draw = function(self)
			love.graphics.setColor(1, .5, 0)
			for a = 1, #self do
				love.graphics.circle("line", self[a].x, self[a].y, self[a].size / 2)
			end
		end,
		expire = function(self, dt)
			self.timer = self.timer + dt
			if self.timer > self.duration and #self > 0 then
				self[#self] = nil
				self.timer = self.timer - self.duration
			end
		end,
		check = function(self)
			for a = 1, #self do for b = 1, #enemies do
				if not enemies[b].dying and collides(trails[a], enemies[b]) then
					if enemies[b].isbullet then
						player:destroy()
					else
						enemies[b]:die()
					end
				end
			end end
		end,
		purge = function(self)
			while #self > 0 do self[#self] = nil end
		end,
	}

	enemies = {
		movements = {
			pass = function(self)
				self.x = self.x + self.speedx
				self.y = self.y + self.speedy
				self.dying = self.dying or
					(self.x < 1 or self.x > screen.w or self.y < 1 or self.y > screen.h)
				return self
			end,
			wander = function(self)
				self.x = self.x + self.speedx
				self.y = self.y + self.speedy
				if self.x < 1 or self.x > screen.w then
					self.speedx = - self.speedx
				end
				if self.y < 1 or self.y > screen.h then
					self.speedy = - self.speedy
				end
				return self
			end,
			chase = function(self)
				self.speedx = self.speedx * 199/200 + (player.x - self.x) / 2000
				self.speedy = self.speedy * 199/200 + (player.y - self.y) / 2000
				self.speedx = self.speedx / math.abs(self.speedx) * math.min(3, math.abs(self.speedx))
				self.speedy = self.speedy / math.abs(self.speedy) * math.min(3, math.abs(self.speedy))
				self.x = self.x + self.speedx
				self.y = self.y + self.speedy
				return self
			end,
		},
		draw = function(self)
			for a = 1, #self do
				love.graphics.setColor(self[a].kind.color)
				love.graphics.rectangle("fill", self[a].x - self[a].size / 2, self[a].y - self[a].size / 2, self[a].size, self[a].size)
			end
		end,
		make = function(self, kind, x, y, speedx, speedy)
			self[#self + 1] = {
				kind = kind, x = x, y = y, speedx = speedx, speedy = speedy,
				size = kind.size, 
				dying = false,
				die = function(self)
					explosions:make(self.x, self.y, self.size)
					game.score = game.score + 1
					self.dying = true
					-- will be disposed of in cleanup
				end,
			}
		end,
		move = function(self)
			for a = 1, #self do
				self[a] = self[a].kind.movement(self[a])
			end
		end,
		check = function(self)
			for a = 1, #self do
				if not self[a].dying and collides(player, self[a]) then
					player:destroy()
					self[a]:die()
				end
				if self[a].shoots_at and game.timer >= self[a].shoots_at then
					self:shoot(a)
					self[a].shoots_at = nil
				end
			end
			if #self == 0 and game.happened == #scenario[game.level] then
				endlevel()
			end
		end,
		shoot = function(self, a)
			print(a, "shoots")
		end,
		cleanup = function(self)
			for a = #self, 1, -1 do if self[a].dying then
				self[a] = self[#self]
				self[#self] = nil
			end end
		end,
	}

	enemies.kinds = {
		wanderer = {
			color = {.8, 0, .4},
			movement = enemies.movements.wander,
			size = player.size,
		},
		chaser = {
			color = {1, 0, 0},
			movement = enemies.movements.chase,
			size = player.size,
		},
		passer = {
			color = {0, .5, .8},
			movement = enemies.movements.pass,
			size = player.size,
		},
		bullet = {
			color = {1, 1, 1},
			movement = enemies.movements.pass,
			size = 5,
		},
	}

	explosions = {
		maxsteps = 15,
		make = function(self, x, y, size)
			self[#self + 1] = {
				step = 0,
				x = x,
				y = y,
				size = size,
			}
		end,
		grow = function(self)
			-- must go backwards not to disrupt the loop
			for a = #self, 1, -1 do
				self[a].step = self[a].step + 1
				if self[a].step > self.maxsteps then
					self:destroy(a)
				end
			end
		end,
		destroy = function(self, a)
			self[a] = self[#self]
			self[#self] = nil
		end,
		draw = function(self)
			love.graphics.setColor(1,1,0)
			for a = 1, #self do
				love.graphics.rectangle("line", self[a].x - self[a].size / 2 - self[a].step * 2,  self[a].y - self[a].size / 2  - self[a].step * 2, self[a].size + self[a].step * 4, self[a].size + self[a].step * 4)
			end
		end,
		purge = function(self)
			while #self > 0 do self[#self] = nil end
		end,
	}

	origins = {
		ul = { x = 1, y = 1 },
		u = { x = screen.w / 2, y = 1 },
		ur = { x = screen.w - 1, y = 1 },
		l = { x = 1, y = screen.h / 2 },
		c = { x = screen.w / 2, y = screen.h / 2 },
		r = { x = screen.w - 1, y = screen.h / 2 },
		dl = { x = 1, y = screen.h - 1 },
		d = { x = screen.w / 2, y = screen.h - 1 },
		dr = { x = screen.w - 1, y = screen.h - 1 },
	}
	
	scenario = {
		--1
		{
			title = "Wave 1",
		player_origin = origins.c,
			--1st enemy
			{ time = 0, origin = origins.ul, speed = { 1, 1 }, kind = enemies.kinds.passer },
			--2nd enemy
			{ time = 2, origin = origins.l, speed = { 1, 0 }, kind = enemies.kinds.passer },
			--3rd enemy
			{ time = 4, origin = origins.dl, speed = { 1, -1 }, kind = enemies.kinds.passer },
			--4th enemy
			{ time = 6, origin = origins.d, speed = { 0, -1 }, kind = enemies.kinds.passer },
		},
		--2
		{
			title = "Wave 2",
		player_origin = origins.d,
			--1st enemy
			{ time = 0, origin = origins.ul, speed = { 1, 1 }, kind = enemies.kinds.wanderer },
			--2nd enemy
			{ time = 1, origin = origins.r, speed = { -5, 0 }, kind = enemies.kinds.bullet },
			--3rd enemy
			{ time = 2, origin = origins.l, speed = { 1, 0 }, kind = enemies.kinds.wanderer },
		},
		--3
		{
			title = "Wave 3",
		player_origin = origins.c,
			--1st enemy
			{ time = 2, origin = origins.u, speed = { 1, 1 }, kind = enemies.kinds.chaser },
			--2nd enemy
			{ time = 2, origin = origins.dr, speed = { 1, 0 }, kind = enemies.kinds.chaser },
			--3rd enemy
			{ time = 3, origin = origins.dl, speed = { 1, 0 }, kind = enemies.kinds.chaser },
		},
		--4 - enemies shoot
		{
			title = "Wave 4",
		player_origin = origins.d,
			--1st enemy
			{ time = 0, origin = origins.ul, speed = { 0, 0 }, kind = enemies.kinds.passer, shoots_in = 2 },
			--2nd enemy
			{ time = 0, origin = origins.ur, speed = { 0, 0 }, kind = enemies.kinds.passer, shoots_in = 2.5 },
			--3rd enemy
			{ time = 0, origin = origins.c, speed = { 0, 0 }, kind = enemies.kinds.passer, shoots_in = 3 },
			-- shoots_after?
			-- range?
		},
	}

	game = {
		timer = 0,
		level = 4,
		happened = 0,
		score = 0,
		mode = "title",
	}
end


function collides(obj1, obj2)
	local o1x, o1y, o2x, o2y, d
	o1x = obj1.x + obj1.size / 2
	o1y = obj1.y + obj1.size / 2
	o2x = obj2.x + obj2.size / 2
	o2y = obj2.y + obj2.size / 2
	d = obj1.size / 2 + obj2.size / 2
	return (o1x - o2x) ^ 2 + (o1y - o2y) ^ 2 < d ^ 2
end


function beginlevel()
	game.timer = 0
	game.mode = "play"
	game.happened = 0
	player.x = scenario[game.level].player_origin.x
	player.y = scenario[game.level].player_origin.y
end


function happen(number)
	game.happened = number
	local s = scenario[game.level][number]
	enemies:make(s.kind, s.origin.x, s.origin.y, s.speed[1], s.speed[2])
	enemies[#enemies].isbullet = s.kind == enemies.kinds.bullet
	enemies[#enemies].shoots_at = game.timer + s.shoots_in
end


function endlevel()
	trails:purge()
	explosions:purge()
	game.timer = 0
	game.level = game.level + 1
	if game.level > #scenario then
		game.mode = "theend"
		game.level = 1
	else
		game.mode = "title"
	end
end


function love.update(dt)
	if dt < 1/50 then
		love.timer.sleep(1/30 - dt)
	end
	game.timer = game.timer + dt
	if game.mode == "play" then
		trails:expire(dt)
		enemies:move()
		enemies:check()
		enemies:cleanup()
		explosions:grow()
		for a = game.happened + 1, #scenario[game.level] do
			if game.timer > scenario[game.level][a].time then
				happen(a)
			end
		end
	elseif game.mode == "title" then
		if game.timer > 3 then
			beginlevel()
		end
	end
end


function osd()
	love.graphics.setColor(1,1,1)
	love.graphics.print(math.floor(game.timer) .. ':' .. game.score, 1, 2)
	love.graphics.print('game.mode = ' .. game.mode, 1, 20)
	love.graphics.print('#enemies = ' .. #enemies, 1, 30)
end


function love.draw()
	if game.mode == "play" then
		trails:draw()
		player:draw()
		enemies:draw()
		explosions:draw()
	elseif game.mode == "title" then
		love.graphics.setColor(1,1,1)
		love.graphics.print(scenario[game.level].title, 100, 100)
	elseif game.mode == "theend" then
		love.graphics.setColor(1,1,1)
		love.graphics.print('The End', 100, 100)
	end
	osd()
end


function love.mousepressed(x, y, button)
	if game.mode == "title" or game.mode == "theend" then
		beginlevel()
	elseif game.mode == "play" then
		player:move(x, y)
		trails:check()
	end
end


function love.keypressed(k)
	if k == "escape" or k == "q" then
		love.event.push("quit")
	end
end
